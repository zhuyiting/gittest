/**
 * 〈一句话功能简述〉<br>
 * 〈hello world!〉
 *
 * @author zhuyiting
 * @create 2019/7/17
 * @since 1.0.0
 */
public class HelloWorld {

    public static void main(String[] args) {
        System.out.println("Hello World!");
    }

}
